//
//  ViewController.m
//  MaxHeart
//
//  Created by Matt Marose on 2/2/16.
//
//

#import "ViewController.h"

@interface ViewController ()


@end

@implementation ViewController

NSArray *goals;
double minHeartRate;
double maxHeartRate;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    goals = @[@"Maintenance (Warm up)", @"Weight Loss (Burn Fat)", @"Increase Stamina/Endurance (Cardio)", @"Hardcore Conditioning", @"Maximum Athlete Training"];
    
}

- (IBAction)calculate:(id)sender {
    if (![self validateFields]) {
        return;
    }
    
    int selectedGoal = [self.goalPicker selectedRowInComponent:0];
    
    switch (selectedGoal) {
        case 0:
            [self setRange: (double)0.5:(double)0.55];
            break;
        case 1:
            [self setRange: (double)0.55:(double)0.65];
            break;
        case 2:
            [self setRange: (double)0.65:(double)0.75];
            break;
        case 3:
            [self setRange: (double)0.75:(double)0.85];
            break;
        case 4:
            [self setRange: (double)0.85:(double)0.95];
            break;
        default:
            break;
    }
    
    
    
    
    NSString *rangeString = [NSString stringWithFormat:@"Your target heart rate zone is %d - %d", (int)minHeartRate, (int)maxHeartRate];
    
    [self showDialog:@"Your results": rangeString];
    
   
    
    
}

-(void) showDialog: (NSString *) title: (NSString *)message {
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:title
                                                    message:message
                                                   delegate:nil
                                          cancelButtonTitle:@"OK"
                                          otherButtonTitles:nil];
    [alert show];
}

- (void) setRange: (double)minMultipler : (double)maxMultipler {
    int selectedFormula = [self.formulaSegment selectedSegmentIndex];
    int age = [self.ageTextField.text intValue];
    int restingHeartRate = [self.restingHeartRateTextField.text intValue];
    int highestHeartRate = 220 - age;
    
    if (selectedFormula == 0) {
        minHeartRate = highestHeartRate * minMultipler;
        maxHeartRate = highestHeartRate * maxMultipler;
    } else {
        minHeartRate = ((highestHeartRate - restingHeartRate) * minMultipler) + restingHeartRate;
        maxHeartRate = ((highestHeartRate - restingHeartRate) * maxMultipler) + restingHeartRate;
    }
}

- (BOOL)validateFields {
    
    if (self.ageTextField.text.length == 0) {
        [self showDialog:@"Oops!": @"Please enter your age"];
        return NO;
    }
    
    if ([self.formulaSegment selectedSegmentIndex] == 1 && self.restingHeartRateTextField.text.length == 0) {
        [self showDialog:@"Oops!": @"For Karvonen formula, please enter your resting heart rate"];
        return NO;
    }
    
    return YES;
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return goals.count;
}

- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return goals[row];
}


- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.ageTextField endEditing:YES];
    [self.restingHeartRateTextField endEditing:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
