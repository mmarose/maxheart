//
//  AppDelegate.h
//  MaxHeart
//
//  Created by Matt Marose on 2/2/16.
//
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

