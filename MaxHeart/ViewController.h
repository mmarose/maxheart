//
//  ViewController.h
//  MaxHeart
//
//  Created by Matt Marose on 2/2/16.
//
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate>
@property (strong, nonatomic) IBOutlet UITextField *ageTextField;
@property (strong, nonatomic) IBOutlet UITextField *restingHeartRateTextField;
@property (strong, nonatomic) IBOutlet UIPickerView *goalPicker;
@property (strong, nonatomic) IBOutlet UISegmentedControl *formulaSegment;


@end

